/**
 * @file LibCodeReleaseProvider.h
 */

#include "Tools/Module/Module.h"
#include "Representations/BehaviorControl/Libraries/LibCodeRelease.h"
#include "Representations/Infrastructure/FrameInfo.h"
#include "Representations/Infrastructure/RobotInfo.h"
#include "Representations/Modeling/BallModel.h"
#include "Representations/Modeling/TeamBallModel.h"
#include "Representations/Modeling/TeamPlayersModel.h"
#include "Representations/Modeling/RobotPose.h"
#include "Representations/Configuration/FieldDimensions.h"
#include "Representations/Communication/TeamData.h"
#include "Representations/BehaviorControl/BehaviorStatus.h"
#include "Representations/Configuration/BehaviorParameters.h"

MODULE(LibCodeReleaseProvider,
{,
  REQUIRES(BallModel),
  REQUIRES(TeamBallModel),
  REQUIRES(TeamPlayersModel),
  REQUIRES(FieldDimensions),
  REQUIRES(FrameInfo),
  REQUIRES(RobotPose),
  REQUIRES(RobotInfo),
  REQUIRES(TeamData),
  REQUIRES(ObstacleModel),
  REQUIRES(BehaviorParameters),
  USES(BehaviorStatus),
  PROVIDES(LibCodeRelease),
});

class LibCodeReleaseProvider : public LibCodeReleaseProviderBase
{
  void update(LibCodeRelease& libCodeRelease);

  private:
    bool prevRivalHasTheBall;
    Vector2f whereToShootL= Vector2f(0,0);
    double distanceToBall;
    Vector2f nearestEnemyL= Vector2f(1000.f,1000.f);
    Vector2f desiredPosL =Vector2f(0,0);
    Teammate closerToTheBall;

    bool isCloserToTheBall();
    Teammate getCloserToTheBall();
    bool ballInOppField();
    bool ballInsideOwnGoal();
    bool keeperInsidePenaltyArea();
    bool mustShootToGoal();
    Vector2f getDesiredPosition();
    Vector2f getBall();
    Vector2f getPlaceToShoot();
    bool isThatClear(Vector2f coordenada,float distanceToBeClear);
    bool willBeClear(Vector2f origen, Vector2f destino);
    bool hasRivalTheBall();
    bool isAboutToShoot();
    float getDistanceToLine(Vector2f left,Vector2f right, float pendiente);

};
