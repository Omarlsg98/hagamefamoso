/* Move the Player till he knows where he is
*  We start by moving the head to the far right, then the far left (We only do this once)
*/
option(FindYourSelf)
{

  common_transition
  {
    if(theRobotPose.validity>=0.7f)
        goto playerLocated;
  }

  /** Starts moving to the right */
  initial_state(StartSearch)
  {
    transition
    {
        goto moveSwitchRight;
    }
  }

  target_state(playerLocated)
  {
    action
    {
      // When we are in this state, it means the player locates himself
    }
  }

  /** Moves the head to the far right, then change the state to moveSwitchLeft */
  state(moveSwitchRight)
  {
     transition
    {
      if(!theHeadMotionEngineOutput.moving && theHeadMotionEngineOutput.pan <= (-pi/4))
        goto moveSwitchLeft;
    }
    action
    {
      SetHeadPanTilt((-pi/4), 0);
    }
  }

  /** Moves the head to the far left, if the ball has still not been found, we enter aborted_state */
  state(moveSwitchLeft)
  {
     transition
    {
      if(!theHeadMotionEngineOutput.moving && theHeadMotionEngineOutput.pan >= (pi/4))
        goto playerNotLocated;
    }
    action
    {
      SetHeadPanTilt((pi/4), 0);
    }
  }

  // If we are here, the player hasn't located himself, so the position of the NAO should be changed
  aborted_state(playerNotLocated)
  {
    transition
    {
      if(action_done)
        goto StartSearch;
    }
    action
    {
      MakeTurn(pi/4);
    }
  }
}
