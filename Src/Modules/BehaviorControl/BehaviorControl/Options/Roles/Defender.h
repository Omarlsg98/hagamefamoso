option(Defender)
{
  initial_state(start)
  {
    transition
    {
      if(state_time > 5000)
        goto followFromDistance;
    }
    action
    {
      LookAround();
    }
  }

  state(followFromDistance)
  {
    transition
    {
      if((theTeamBallModel.timeWhenLastValid - theFrameInfo.time) > 300)
        goto whereIsIt;
      if(theTeamBallModel.position.x() < 150)
        goto geneticDefense;
    }
    action
    {
      HeadControlMode(HeadControl::lookForward);
      Vector2f going2;

      if(theRobotInfo.number == 2){
        if(theRobotPose.translation.y() < 2700 && theRobotPose.translation.y() > -1700){
          going2 = {-1600,theTeamBallModel.position.y()+1000};
          WalkToTarget(Pose2f(50.f, 50.f, 50.f), Pose2f((theRobotPose.inversePose * ((Vector2f){theTeamBallModel.position.x(),theTeamBallModel.position.y()})).angle(), theRobotPose.inversePose * going2));
        }
      }
      else if(theRobotInfo.number == 3){
        if(theRobotPose.translation.y() > -2700 && theRobotPose.translation.y() < 1700){
          going2 = {-1600,theTeamBallModel.position.y()-1000};
          WalkToTarget(Pose2f(50.f, 50.f, 50.f), Pose2f((theRobotPose.inversePose * ((Vector2f){theTeamBallModel.position.x(),theTeamBallModel.position.y()})).angle(), theRobotPose.inversePose * going2));
        }
      }
    }
  }

  state(geneticDefense)
  {
    transition
    {
      if((theTeamBallModel.timeWhenLastValid - theFrameInfo.time) > 300)
        goto whereIsIt;
      if(theTeamBallModel.position.x() > 150)
        goto followFromDistance;
      //Si estoy en la trayectoria
      //Ry = (Rx*(vector[1]/vector[0])) + (punto[1] + (-1 * punto[0] * (vector[1]/vector[0])));

      Vector2f punto = {theBallModel.estimate.position.y(),theBallModel.estimate.position.x()};
      Vector2f vector = {theBallModel.estimate.velocity.y(), theBallModel.estimate.velocity.x()};

      if(std::abs(vector[0]) > 0.5){
        float pendiente = vector[1]/vector[0];
        float intersecto = (punto[1] + (-1 * punto[0] * (vector[1]/vector[0])));

        float distancia = (float)(std::abs(intersecto) / (sqrt(pow(pendiente,2) + 1)));

        if(theBallModel.estimate.velocity.x() < -300)
          if(distancia < 400)
            goto fallDefence;
      }
    }
    action
    {
      HeadControlMode(HeadControl::lookForward);
      WalkToTarget(Pose2f(1.f, 1.f, 1.f), theGeneticLocator.activation(true, (int)theFrameInfo.time));
    }
  }

  state(whereIsIt)
  {
    transition
    {
      if((theTeamBallModel.timeWhenLastValid - theFrameInfo.time) < 300)
        goto followFromDistance;
    }
    action
    {
      HeadControlMode(HeadControl::lookForward);
      WalkAtRelativeSpeed(Pose2f(1.f, 0.f, 0.f));
    }
  }

  state(attack4defence)
  {
    transition
    {
      if((theTeamBallModel.timeWhenLastValid - theFrameInfo.time) < 300)
        goto followFromDistance;
    }
    action
    {
      HeadControlMode(HeadControl::lookForward);
      WalkAtRelativeSpeed(Pose2f(1.f, 0.f, 0.f));
    }
  }

  state(fallDefence)
  {
    transition
    {
      if(action_done && state_time > 1500)
        goto getUp;
    }
    action
    {
      SpecialAction(SpecialActionRequest::preventBall);
    }
  }

  state(getUp)
  {
    transition
    {
      if(theFallDownState.state == FallDownState::upright){
        if(theBallModel.estimate.position.x() <= 350)
          goto throwItAway;
        else
          goto geneticDefense;
      }
    }
    action
    {
      GetUp();
    }
  }

  state(throwItAway)
  {
    transition
    {
      if(theBallModel.estimate.position.x() > 350)
        goto followFromDistance;
    }
    action
    {
      Vector2f target=Vector2f(0.f,0.f);
          

      
          target = theLibCodeRelease.bestTeammateForPass();
     
         theMotionRequest = thePathPlanner.plan(Pose2f(0.f,theLibCodeRelease.ball),Pose2f(0.6f,0.6f,0.6f),false,true);
    
         ShootAt(target,false);
    }
  }

}
