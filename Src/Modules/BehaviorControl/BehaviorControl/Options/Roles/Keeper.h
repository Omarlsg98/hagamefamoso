option(Keeper)
{
	initial_state(start)
	{
		transition
		{
			if(state_time > 1000)
				goto lookForBall;
			if(theLibCodeRelease.timeSinceBallWasSeen < 300){
				if (theLibCodeRelease.ballInOppField == true && state_time > 10000){
					goto oppSide;
				} else {
					if (theLibCodeRelease.keeperInsideGoal == false && state_time > 10000)
						goto returnToArea;
					if (theLibCodeRelease.keeperInsideGoal == true)
						goto alignToBall;
					}
				}
	 	}
		action
		{
			Stand();
		}
	}
  state(lookForBall)
	{
		transition
		{
			if (std::abs(theLibCodeRelease.angleToOppGoal) > 5_deg) {
				goto returnToArea;
			}
			if (state_time > 5000) {
        if (theLibCodeRelease.ballInOppField == true && state_time > 5000 )
        {
					goto oppSide;
        } else {
						if (theLibCodeRelease.keeperInsideGoal == false && state_time > 5000) {
							goto returnToArea;
						}
						if (theLibCodeRelease.keeperInsideGoal == true && state_time > 5000) {
							goto alignToBall;
						}
					}
				}
  	}
		action
		{

			LookAround();
		}
	}
	state(alignToBall)
		{
		    transition
		    {
		        if (theLibCodeRelease.timeSinceBallWasSeen > 8000)
		            goto lookForBall;
		        if (theLibCodeRelease.ballInOppField == true && theLibCodeRelease.keeperInsideGoal == false) {
		            goto returnToArea;
		        }
		        else {
		            if (theLibCodeRelease.ballInOppField == true) {
		                goto oppSide;
		            }
		            else {
		                if (theLibCodeRelease.keeperInsideGoal == false) {
		                    goto returnToArea;
		                }
		                else {
		                    if (theLibCodeRelease.ballInsideOwnGoal == true) {
													if (theBallModel.estimate.velocity.x() > -80) {
															goto alignBehindBall;
													} else {
		                        if (100 > theBallModel.estimate.position.y() && -100 < theBallModel.estimate.position.y()
		                            && theBallModel.estimate.velocity.x() < -400)
		                            goto goDown;
													}

		                    }
		                    else {
		                        if (-100 > theBallModel.estimate.position.y() && theBallModel.estimate.velocity.x() < -250
		                            && theBallModel.estimate.position.x() < 900)
		                            goto PreventRight;
		                        if (100 < theBallModel.estimate.position.y() && theBallModel.estimate.velocity.x() < -250
		                            && theBallModel.estimate.position.x() < 900)
		                            goto PreventLeft;
		                        if (100 > theBallModel.estimate.position.y() && -100 < theBallModel.estimate.position.y()
		                            && theBallModel.estimate.velocity.x() < -300)
		                            goto goDown;

		                        else {
		                            goto alignToBall;
		                        }
		                    }
		                }
		            }
		        }
		    }
		    action
		    {
					HeadControlMode(HeadControl::lookForward);
					if (theBallModel.estimate.position.x() < theFieldDimensions.xPosOwnPenaltyArea){
						WalkToTarget(Pose2f(100.f, 100.f, 100.f),
						Pose2f((theRobotPose.inversePose * Vector2f(theFieldDimensions.xPosOpponentPenaltyArea, 0)).angle(),
						theBallModel.estimate.position.x(),
						theBallModel.estimate.position.y()));
					}
					else{
						WalkToTarget(Pose2f(100.f, 100.f, 100.f),
						Pose2f((theRobotPose.inversePose * Vector2f(theFieldDimensions.xPosOpponentPenaltyArea, 0)).angle(),
						(theRobotPose.inversePose * Vector2f(theFieldDimensions.xPosOwnGroundline + 200.f, 0)).x(),
						theBallModel.estimate.position.y()));
					}
		    }
		}

	state(returnToArea)
  {
    transition
    {
			if (theLibCodeRelease.keeperInsideGoal == true && std::abs(theLibCodeRelease.angleToOppGoal) < 2_deg)
					goto lookForBall;

    }
    action
    {
			WalkToTarget(Pose2f(100.f, 100.f, 100.f),Pose2f((theRobotPose.inversePose * Vector2f(theFieldDimensions.xPosOpponentPenaltyArea, 0)).angle(),
			(theRobotPose.inversePose * Vector2f(theFieldDimensions.xPosOwnGroundline + 150.f, 0)).x(),
			(theRobotPose.inversePose * Vector2f(theFieldDimensions.yPosRightPenaltyArea + 200.f, 0)).y()));
			LookAround();
		}
  }

	state(oppSide)
  {
    transition
    {
			if (theLibCodeRelease.keeperInsideGoal == false)
				goto returnToArea;
			if (theLibCodeRelease.ballInOppField == false ) {
				if (theLibCodeRelease.keeperInsideGoal == false) {
					goto returnToArea;
				}else {
					goto alignToBall;
					}
				}
	    if( 100 > theBallModel.estimate.position.y() && -100 < theBallModel.estimate.position.y()
	      && theBallModel.estimate.velocity.x() < -400 && 2500 > theBallModel.estimate.position.x())
	      goto goDown;
			if (-100 > theBallModel.estimate.position.y() && theBallModel.estimate.velocity.x() < -250
						&& theBallModel.estimate.position.x() < 900)
						goto PreventRight;
			if (100 < theBallModel.estimate.position.y() && theBallModel.estimate.velocity.x() < -250
						&& theBallModel.estimate.position.x() < 900)
						goto PreventLeft;
    }
    action
    {
      Stand();
			LookAround();
    }
  }

  state(PreventRight)
  {
    transition
    {
      if (state_time > 100 && action_done)
				goto lookForBall;
    }
    action
    {
      SpecialAction(SpecialActionRequest::rightDive);
    }
  }

	state(alignBehindBall)
  {
    transition
    {
      if(theLibCodeRelease.timeSinceBallWasSeen > theBehaviorParameters.ballNotSeenTimeOut)
        goto lookForBall;
			if (theLibCodeRelease.keeperInsideGoal==false) {
				goto returnToArea;
			}
      if(theLibCodeRelease.between(theBallModel.estimate.position.y(), 20.f, 50.f)
         && theLibCodeRelease.between(theBallModel.estimate.position.x(), 140.f, 200.f)
         && std::abs(theLibCodeRelease.angleToOppGoal) < 2_deg)
        goto kick;
    }
    action
    {
      theHeadControlMode = HeadControl::lookForward;
      WalkToTarget(Pose2f(0.5f, 0.5f, 0.5f), Pose2f(theLibCodeRelease.angleToOppGoal,
				theBallModel.estimate.position.x() - 200.f, theBallModel.estimate.position.y() - 30.f));
    }
  }

  state(kick)
  {
    transition
    {
      if(state_time > 3000 || (state_time > 10 && action_done))
        goto lookForBall;
    }
    action
    {
      InWalkKick(WalkKickVariant(WalkKicks::forward, Legs::left), Pose2f(theLibCodeRelease.angleToOppGoal,theBallModel.estimate.position.x() - 160.f, theBallModel.estimate.position.y() - 55.f));
		}

    }


  state(PreventLeft)
  {
    transition
    {
      if (state_time > 100 && action_done)
				goto lookForBall;
    }
    action
    {
      SpecialAction(SpecialActionRequest::leftDive);
    }
  }

  state(goDown)
  {
    transition
    {
      if (state_time > 100 && action_done)
				goto lookForBall;
    }
    action
    {
      SpecialAction(SpecialActionRequest::preventBall);
    }
  }

}
