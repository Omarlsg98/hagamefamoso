/** 
Habilidad para ponerse en la posicion adecuada para pegarle hacia un lado al balon:
Recibe: 
  conIzquierda: para saber si pegar de izquierda a derecha o lo contrario
-------------------------!!!!!!El robot debe estar cerca a la pelota!!!!!-----------------------
*/


option(StealBallWith, (const bool) conIzquierda)
{
  initial_state(initial)
  {
  	transition
  	{
      if(theBallModel.estimate.position.norm() < (theBehaviorParameters.distanceToGoShoot+100.f)){
        goto alignBehindBall;
      }else{
        goto abortedState;
      }
  	}
    action
    {
    }
  }

  aborted_state(abortedState){
    transition{
      goto initial;
    }
  }


  state(alignBehindBall)
  {
    transition
    {
      if(theLibCodeRelease.between(theBallModel.estimate.position.y(), 20.f - conIzquierda*70, 50.f-conIzquierda*70)
        && theLibCodeRelease.between(theBallModel.estimate.position.x(), 150.f, 160.f)
        && std::abs(theRobotPose.rotation) < 2_deg)
        goto kick;
    }
    action
    {
      WalkToTarget(Pose2f(0.5f,0.5f, 0.5f), Pose2f(-theRobotPose.rotation, theBallModel.estimate.position.x() - 155.f, theBallModel.estimate.position.y() -36.f +72*conIzquierda));
    }
  }

  target_state(kick)
  {
    transition
    {
      if(action_done || action_aborted)
       goto initial;
    }
    action
    {
      if (conIzquierda){
        InWalkKick(WalkKickVariant(WalkKicks::sidewardsInner, Legs::left), Pose2f(0.f, theBallModel.estimate.position.x() , theBallModel.estimate.position.y() - 30.f));
      }else{
        InWalkKick(WalkKickVariant(WalkKicks::sidewardsInner, Legs::right), Pose2f(0.f, theBallModel.estimate.position.x() , theBallModel.estimate.position.y() + 30.f));
      }

    /*
      InWalkKick(WalkKickVariant(WalkKicks::forwardShoot, Legs::left), Pose2f(angleToTarget, theBallModel.estimate.position.x() - 160.f, theBallModel.estimate.position.y() - 55.f));
    */
    }
  }  
}