option(ChooseRole)
{
  initial_state(elegir)
  {
    transition
    {
      if(theRobotInfo.number == 1)
        goto esPortero;
      else if(theBehaviorStatus.role == Role::defender)
        goto esDefensa;
      else if(theBehaviorStatus.role == Role::striker || theBehaviorStatus.role == Role::supporter)
        goto esDelantero;
    }
    action
    {
        if(theBehaviorStatus.role == Role::undefined)
      {
        if(theRobotInfo.number == 1)
          theBehaviorStatus.role = Role::keeper;
        else if(theRobotInfo.number == 2)
          theBehaviorStatus.role = Role::striker;
        else if(theRobotInfo.number == 3)
          theBehaviorStatus.role = Role::striker;
        else if(theRobotInfo.number == 4)
          theBehaviorStatus.role = Role::striker;
        else if(theRobotInfo.number == 5)
          theBehaviorStatus.role = Role::striker;
        else if(theRobotInfo.number == 6)
          theBehaviorStatus.role = Role::striker;
      }
      LookForward();
    }
  }

  state(esPortero)
  {
    action
    {
      theBehaviorStatus.role = Role::keeper;
      LookAround();
      //Keeper();
    }
  }
  
  state(esDefensa)
  {
    transition
    {
       /* if(theLibCodeRelease.nbOfStriker == 0)
          goto esDelantero;*/
    }
    action
    {
      theBehaviorStatus.role = Role::defender;
      SpecialAction(SpecialActionRequest::sitDown);
      //LookForward();
      //Defender();
    }
  }

  state(esDelantero)
  {
    transition
    {
      
    }
    action
    {
      Gallina();
      theBehaviorStatus.role = Role::striker;
      OffensiveSkills();
    }
  }

  /*state(PlaySupporter)
  {
    transition
    {
      if(action_aborted) {
        if (theLibCodeRelease.nbOfDefender <= 1)
          goto PlayDefender;
        else if (theLibCodeRelease.closerToTheBall)
          goto PlayStriker;
        else{}
      }
    }
    action
    {
      theBehaviorStatus.role = Role::supporter;
      LookForward();
      Supporter();
    }
  }*/
}
