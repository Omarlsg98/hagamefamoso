#include "Tools/Module/Module.h"
#include "Representations/Modeling/TeamBallModel.h"
#include "Representations/Modeling/GeneticLocator.h"
#include "Representations/Modeling/RobotPose.h"
#include "Representations/Communication/TeamData.h"
#include "Representations/Modeling/BallModel.h"

MODULE(GeneticLocatorProvider,
{,
  REQUIRES(TeamData),
  REQUIRES(TeamBallModel),
  REQUIRES(RobotPose),
  REQUIRES(BallModel),
  REQUIRES(GeneticLocator),
  PROVIDES(GeneticLocator),
});

class GeneticLocatorProvider : public GeneticLocatorProviderBase
{
public:
  GeneticLocator* geneticLocatorr;
	GeneticLocatorProvider();
	float result(int a, int b);
  std::vector<std::vector<float>> ordenador(int a, int b);

private:
	void update(GeneticLocator& geneticLocator);
};
