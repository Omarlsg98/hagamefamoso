#include "Tools/Module/Module.h"
#include "Representations/Modeling/TeamBallModel.h"
#include "Representations/Modeling/GenDispose.h"
#include "Representations/Modeling/RobotPose.h"
#include "Representations/Communication/TeamData.h"
#include "Representations/Modeling/BallModel.h"

MODULE(GenDisposeProvider,
{,
  REQUIRES(TeamData),
  REQUIRES(TeamBallModel),
  REQUIRES(RobotPose),
  REQUIRES(BallModel),
  REQUIRES(GenDispose),
  PROVIDES(GenDispose),
});

class GenDisposeProvider : public GenDisposeProviderBase
{
public:
	GenDisposeProvider();

private:
	void update(GenDispose& genDispose);
};
