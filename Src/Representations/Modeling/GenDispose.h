/**
 * @file LibCodeRelease.h
 */

#pragma once
#include "Tools/Math/Pose2f.h"
#include "Tools/Function.h"

#include "Representations/Communication/BHumanTeamMessageParts/BHumanMessageParticle.h"

#include "Representations/Communication/BHumanMessage.h"

STREAMABLE(GenDispose, COMMA public BHumanMessageParticle<idGenDispose>
{
  /** BHumanMessageParticle functions */
  void operator >> (BHumanMessage& m) const override;
  void operator << (const BHumanMessage& m) override,

  (int) Hmtimes,
});
