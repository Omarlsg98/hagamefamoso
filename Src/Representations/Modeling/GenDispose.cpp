/**
 * @file BallModel.cpp
 * Implementation of the BallModel's drawing functions
 */

#include "GenDispose.h"
#include "Platform/Time.h"
#include "Representations/Configuration/BallSpecification.h"
#include "Representations/Infrastructure/FrameInfo.h"
#include "Tools/Debugging/DebugDrawings.h"
#include "Tools/Debugging/DebugDrawings3D.h"
#include "Tools/Math/Approx.h"
#include "Tools/Modeling/BallPhysics.h"
#include "Tools/Module/Blackboard.h"

void GenDispose::operator >> (BHumanMessage& m) const
{
    // m.theBSPLStandardMessage.Hmtimes = Hmtimes;
}

void GenDispose::operator << (const BHumanMessage& m)
{
    // Hmtimes = m.theBSPLStandardMessage.Hmtimes;
}
