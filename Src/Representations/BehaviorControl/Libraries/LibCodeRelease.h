/**
 * @file LibCodeRelease.h
 */

#pragma once
#include "Tools/Function.h"
#include "Tools/Math/Eigen.h"
#include "Tools/Streams/AutoStreamable.h"
#include "Tools/Streams/Enum.h"

STREAMABLE(LibCodeRelease,
{
	FUNCTION(bool(float value, float min, float max)) between;
	FUNCTION(float(float value, float min, float max)) clamp;
	FUNCTION(Vector2f(float angle,Vector2f coordenada)) rotateCoordenates;
	FUNCTION(bool(Vector2f coordenada,float distanceToBeClear)) isThatClear;
	FUNCTION(Vector2f()) bestTeammateForPass;
	FUNCTION(void(bool isMissing)) setMissingTeammate;
	FUNCTION(int()) getNumberWithinRole,

	(float)(0.f) angleToOppGoal,
	(float)(0.f) angleToOwnGoal,
	(int)(0) timeSinceBallWasSeen,
	(bool)(false) closerToTheBall,
	(bool)(false) ballInOppField,
	(bool)(false) ballInsideOwnGoal,
	(bool)(false) keeperInsideGoal,
	(bool)(false) shootToGoal,
	(bool)(false) theRivalHasTheBall,
	(Vector2f)(Vector2f(0,0)) desiredPos,
	(Vector2f)(Vector2f(0,0)) ball,
	(Vector2f)(Vector2f(4500,0.f)) whereToShoot,
	(Vector2f)(Vector2f(1000.f,1000.f)) nearestEnemy, //Global Coordenates of the neares Enemy to the ball
	(bool) (false) areShooting,
});
